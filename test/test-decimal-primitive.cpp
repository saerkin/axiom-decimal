#define CATCH_CONFIG_MAIN
#include <catch.hpp>
#include <axiom/decimal.h>

using namespace axiom;

TEST_CASE("cstring can be constructed without params", "[axiom::cstring]") {
  cstring str;
  REQUIRE(str.isEmpty());
}

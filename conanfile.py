# noinspection PyUnresolvedReferences
from conans import ConanFile, CMake

class AxiomDecimalConan(ConanFile):
  name = "axiom.decimal"
  version = "0.1"
  license = "MIT"
  author = "Sergey A. Erkin (sergey.a.erkin@gmail.com)"
  url = "https://saerkin@bitbucket.org/saerkin/axiom.decimal.git"
  settings = "os", "compiler", "build_type", "arch"
  options = {"shared": [False]}
  default_options = {"shared": False}
  generators = "cmake"
  exports_sources = "CMakeLists.txt", ".cmake/*", "include/*", "src/*", "test/*"
  no_copy_source = True
  requires = \
    "axiom.string/1.0@sergey-erkin/experimental", \
    "axiom.shared_ptr/1.0@sergey-erkin/experimental"
  build_requires = \
    "Catch/1.12.1@bincrafters/stable"

  def build(self):
    cmake = CMake(self)
    cmake.configure(source_folder="")
    cmake.build()

  def package(self):
    self.copy("*.h", dst="include", src="include")
    self.copy("*.lib", dst="lib", keep_path=False)
    self.copy("*.dll", dst="bin", keep_path=False)
    self.copy("*.dylib*", dst="lib", keep_path=False)
    self.copy("*.so", dst="lib", keep_path=False)
    self.copy("*.a", dst="lib", keep_path=False)

  def package_info(self):
    self.cpp_info.includedirs = ["include"]
    self.cpp_info.srcdirs = ["src"]
